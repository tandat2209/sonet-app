# Sonet App

## 1.  Purpose
* Create a connection between football-players and football fields.
* Easy to search for football fields in the surrounding area.
* Help user to join matches or tournaments easily.

## 2.  Technologies
* Express NodeJS
* React
* Redux
* Firebase

## 3. Things learned when build this app
* [yargs]() command line args
* webpack.config
    - output
        + path: path to store file on disk
        + publicPath: path to reference file
        + dev-server does not create file on disk --> `webpack --watch` 
* [HtmlWebpackPlugin](https://github.com/ampedandwired/html-webpack-plugin)
  -  to inject bundle.js to file html 
* [react-hot-loader](http://gaearon.github.io/react-hot-loader/getstarted/)
  - add `'react-hot'` into `loaders` in webpack.config
  - hot update react
* [babel-plugin-webpack-loaders](https://github.com/istarkov/babel-plugin-webpack-loaders)
    -  replace `require, import` with webpack loader result
    -  `index.js`
    -  `webpack.config.babel.js`
