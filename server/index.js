import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';

import webpack from 'webpack';
import config, { appCssPath } from '../webpack.config.dev';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

const app = express();

// Run Webpack dev server in development mode
if (process.env.NODE_ENV === 'development') {
  const compiler = webpack(config);
  app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }));
  app.use(webpackHotMiddleware(compiler));
}

app.use(bodyParser.json({ limit: '20mb' }));
app.use(bodyParser.urlencoded({ limit: '20mb', extended: false }));

import { Provider } from 'react-redux';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { match, RouterContext } from 'react-router';
import routes from '../client/routes';
import configureStore from '../client/configureStore';

const renderPage = (html, initialState) => {
  return `
    <!doctype html>
    <html>
      <head>
        <title> Sonet App </title>
        <link rel="stylesheet" href="${appCssPath}" />
      </head>
      <body>
        <div id="root">${html}</div>
        <script>
          window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};
        </script>
        <script src="${config.output.publicPath ? config.output.publicPath + "/" : ""}${config.output.filename}"></script>
      </body>
    </html>
  `
}


app.use((req, res, next)=>{
  match({ routes, location: req.url }, (err, redirectLocation, renderProps) => {
    if (err) {
      return res.status(500).end(renderError(err));
    }
    if (redirectLocation) {
      return res.redirect(302, redirectLocation.pathname + redirectLocation.search);
    }
    if (!renderProps) {
      return next();
    }
    const store = configureStore();
    const initialView = renderToString(
      <Provider store={store}>
        <RouterContext {...renderProps} />
      </Provider>
    );
    const finalState = store.getState();

    try{
      const html = renderPage(initialView, finalState);
      const status = renderProps.routes.some(route => route.path === '*') ? 404 : 200;
      res.status(status).send(html);  
    } catch(err){
      next(err);
    }
  })
})

app.listen(8000, (error) => {
  if (!error) {
    console.log(`Sonet is running on 8000!`);
  }
});

export default app;