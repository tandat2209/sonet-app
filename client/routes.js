import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/app/App.react';
import SignInPage from './components/auth/SignInPage.react';
import FormPage from './components/form/FormPage.react';
import NotFound from './components/app/NotFound.react';

const routes = (
  <Route path="/" component={ App }>
    <Route path="sign-in" component={ SignInPage } />
    <Route path="form" component={ FormPage } />
    <Route path="*" component={ NotFound } />
  </Route>
)

export default routes;
