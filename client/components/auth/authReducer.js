export default function authReducer(state={}, action){
  switch(action.type){
    case 'SIGN_IN_SUCCESS':
      return {...state, user: action.payload}
    case 'SIGN_IN_ERROR':
      return {...state, user: null, error: action.payload }
    default:
      return state;
  }
}
