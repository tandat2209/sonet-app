import firebase from 'firebase';
import { firebaseAuth } from '../../lib/firebase';
function authenticate(provider){
  return dispatch => {
    firebaseAuth.signInWithPopup(provider)
      .then(result => dispatch(signInSuccess(result)))
      .catch(error => dispatch(signInError(error)))
  }
}

export function signInError(error){
  return {
    type: 'SIGN_IN_ERROR',
    payload: error
  }
}

export function signInSuccess(result){
  return {
    type: 'SIGN_IN_SUCCESS',
    payload: result.user
  }
}

export function signInWithFacebook(){
  return authenticate(new firebase.auth.FacebookAuthProvider());
}

export function signInWithGoogle(){
  return authenticate(new firebase.auth.GoogleAuthProvider());
}

export function signInWithGithub(){
  return authenticate(new firebase.auth.GithubAuthProvider());
}
