import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import { signInWithFacebook, signInWithGoogle, signInWithGithub } from './authActions';

class SignInPage extends React.Component {
  constructor(props) {
    super(props);
    this.handleSignInWithFacebook = this.handleSignInWithFacebook.bind(this);
    this.handleSignInWithGoogle = this.handleSignInWithGoogle.bind(this);
    this.handleSignInWithGithub = this.handleSignInWithGithub.bind(this);
  }

  handleSignInWithFacebook(){
    this.props.dispatch(signInWithFacebook());
  }

  handleSignInWithGithub(){
    this.props.dispatch(signInWithGithub());
  }

  handleSignInWithGoogle(){
    this.props.dispatch(signInWithGoogle());
  }

  render() {
    const { dispatch, auth } = this.props;
    const loginButtons = auth.user ?
      <pre>{JSON.stringify(auth.user, null, 2)}</pre> :
      <div className="buttons">
        <button onClick={this.handleSignInWithGithub}>Github</button>
        <button onClick={this.handleSignInWithFacebook}>Facebook</button>
        <button onClick={this.handleSignInWithGoogle}>Google</button>
      </div>;
    const loginErrors = auth.error ? <pre>{JSON.stringify(auth.error, null, 2)}</pre> : null
    return (
      <div className="sign-in">
        { loginButtons }
        { loginErrors }
      </div>);
  }
}

SignInPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect(state=>({ auth: state.auth }))(SignInPage);
