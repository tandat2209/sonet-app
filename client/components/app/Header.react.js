import React, {PropTypes} from 'react';
import { Link } from 'react-router';

export default class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="header">
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="form">Form</Link></li>
          <li><Link to="sign-in">Sign in</Link></li>
        </ul>
      </div>
    );
  }
}

Header.propTypes = {
};
