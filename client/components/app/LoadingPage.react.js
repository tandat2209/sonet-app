import styles from './LoadingPage.scss';
import React, { Component } from 'react';

export default class LoadingPage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={styles.loader}>
        <div className={styles.loader_ball}></div>
        <div className={styles.loader_text}>Sonet is loading ...</div>
      </div>
    );
  }
}
