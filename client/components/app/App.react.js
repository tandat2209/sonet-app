import React from 'react';
import LoadingPage from './LoadingPage.react';
import Header from './Header.react';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    // avoid different code between client and server
    this.state = { isMounted: false };
  }

  componentDidMount() {
    this.setState({
      isMounted: true
    })
  }

  render() {
    return (
      <div className="app">
        <Header />
        {this.props.children}
      </div>
    )
  }
}
