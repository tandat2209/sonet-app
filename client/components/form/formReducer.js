export default function formReducer(state={}, action){
  switch (action.type) {
    case 'FORM_SUBMIT_START':
    case 'GET_FORM_MODEL_START':
      return {...state, isLoading: true}

    case 'FORM_SUBMIT_SUCCESS':
    case 'GET_FORM_MODEL_SUCCESS':
      return {...state, isLoading: false, ...action.data}

    case 'FORM_SUBMIT_ERROR':
    case 'GET_FORM_MODEL_ERROR':
      return {...state, isLoading: false, error: action.error}

    default:
      return state;
  }
}
