import { firebaseDb } from '../../lib/firebase';

function formSubmitStart(){
  return {
    type: 'FORM_SUBMIT_START'
  }
}

function formSubmitSuccess(snapshot){
  return {
    type: 'FORM_SUBMIT_SUCCESS',
    data: snapshot
  }
}

function formSubmitError(error){
  return {
    type: 'FORM_SUBMIT_ERROR',
    error
  }
}

export function formSubmit(formModel){
  return dispatch => {
    dispatch(formSubmitStart())
    firebaseDb.ref('form').set(formModel)
      .then(()=>{
        dispatch(formSubmitSuccess(formModel))
      })
      .catch(error=>{
        dispatch(formSubmitError(error))
      })
  }
}

function getFormModelStart(){
  return {
    type: 'GET_FORM_MODEL_START'
  }
}

function getFormModelSuccess(data){
  return {
    type: 'GET_FORM_MODEL_SUCCESS',
    data
  }
}

function getFormModelError(error){
  return {
    type: 'GET_FORM_MODEL_ERROR',
    error
  }
}

export function getFormModel(){
  return dispatch => {
    dispatch(getFormModelStart());
    firebaseDb.ref('form').on('value', snapshot=>{
      dispatch(getFormModelSuccess(snapshot.val()))
    }, error=>dispatch(getFormModelError(error)))
  }
}
