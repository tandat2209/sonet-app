import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import { formSubmit, getFormModel } from './formActions';
import { firebaseDb } from '../../lib/firebase';

class FormPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      hasCreditCard: false,
      gender: 'other',
      selectedNumber: 1
    }
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onFormSubmit(e){
    e.preventDefault();
    const {dispatch} = this.props;
    console.log(e.target.gender.value);
    const formModel = {
      text: e.target.text.value,
      hasCreditCard: e.target.hasCreditCard.checked,
      gender: e.target.gender.value,
      selectedNumber: e.target.selectedNumber.value
    }
    dispatch(formSubmit(formModel))
  }

  componentWillMount(){
    const {dispatch, form} = this.props;
    dispatch(getFormModel());
  }

  componentWillReceiveProps(nextProps){
    this.setState({...nextProps.form});
  }

  componentWillUnmount(){
    // unsubcribe event 'value' from firebase
    firebaseDb.ref('form').off();
  }

  render() {
    const { form } = this.props;
    const { text, hasCreditCard, gender, selectedNumber } = this.state;
    return (
      <form onSubmit={this.onFormSubmit}>
        <fieldset disabled={form.isLoading}>
          <h3>Input Text</h3>
          {/* value does not update, use controlled-component https://facebook.github.io/react/docs/forms.html#controlled-components */}
          <input type="text" placeholder="Input some text" value={text} onChange={e=>{this.setState({text: e.target.value})}} name="text"/>
          <h3>Checkboxes</h3>
          <label><input type="checkbox" checked={hasCreditCard} onChange={e=>{this.setState({hasCreditCard: e.target.checked})}} name="hasCreditCard" /> Has Credit Card</label>

          {/* uncontrolled-component */}
          <h3>Radios</h3>
          <label>
            <input type="radio" defaultChecked={form.gender === 'male'} value="male" name="gender"/> Male
          </label>
          <label>
            <input type="radio" defaultChecked={form.gender === 'female'} value="female" name="gender"/> Female
          </label>
          <label>
            <input type="radio" defaultChecked={form.gender === 'other'} value="other" name="gender"/> Other
          </label>

          <h3>Select</h3>
          <select name="selectedNumber" defaultValue={form.selectedNumber}>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
          </select>
          <pre>{JSON.stringify({form}, null, 2)}</pre>
          <button>Submit</button>
        </fieldset>
      </form>
    );
  }
}

FormPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect(state=>({form: state.form}))(FormPage);
