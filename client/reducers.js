import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import auth from './components/auth/authReducer';
import form from './components/form/formReducer';

const rootReducer = combineReducers({
  auth,
  form,
  routing
})

export default rootReducer;
