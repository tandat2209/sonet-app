/*
  this config is for babel-plugin-webpack-loaders for server rendering of assets included through webpack
  use for server nodemon
  TODO: what if combine webpack.config.babel.js and webpack.config.dev.js
*/

// to move css in external stylesheet
var ExtractTextPlugin = require('extract-text-webpack-plugin'); // eslint-disable-line no-var

module.exports = {
  output: {
    // YOU NEED TO SET libraryTarget: 'commonjs2'
    libraryTarget: 'commonjs2',
  },
  module: {
    loaders: [
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract(
          'style-loader',
          [
            // 'css-loader?modules&importLoaders=2&localIdentName=[name]__[local]--[hash:base64:5]',
            'css-loader?modules&importLoaders=2&localIdentName=[local]',
            'postcss-loader',
            'sass-loader',
          ]
        ),
      },
      // {
      //   test: /\.css$/,
      //   loader: ExtractTextPlugin.extract(
      //     'style-loader',
      //     [
      //       'css-loader?modules&importLoaders=2&localIdentName=[name]__[local]--[hash:base64:5]',
      //       'postcss-loader',
      //     ]
      //   ),
      // },
    ],
  },
  plugins: [
    new ExtractTextPlugin('styles.css', {
        allChunks: true
    })
  ]
};
