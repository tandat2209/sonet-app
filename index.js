var yargs = require('yargs');

const argv = yargs.alias('p', 'production').argv;

if(argv.production){
  // production here
  process.env.NODE_ENV = 'production';
  console.log('Not support yet... ');
  process.exit(1);
} else {
  process.env.NODE_ENV = 'development';
  // Babel polyfill to convert ES6 code in runtime
  require('babel-register')({
    "plugins": [
      [
        // plugin-webpack-loaders to replace `require, import` with webpack loader result
        "babel-plugin-webpack-loaders",
        {
          "config": "./webpack.config.babel.js",
          "verbose": false
        }
      ]
    ]
  });
  require('babel-polyfill');

  require('./server/index');
}
